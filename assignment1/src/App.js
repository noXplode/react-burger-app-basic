import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

class App extends Component {
  state = {
    username: 'Andrew'
  }

  inputChangedHandler = (event) => {
    this.setState( {
      username: event.target.value
    } )
    console.log(this.state)
  }

  render() {
    return (
      <div className="App">
        <UserInput changed={this.inputChangedHandler} name={this.state.username}/>
        <UserOutput name={this.state.username}/>
        <UserOutput name={this.state.username}/>
        <UserOutput name={this.state.username}/>
      </div>
    );
  }
}

export default App;
