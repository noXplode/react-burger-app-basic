import React from 'react';
import './UserOutput.css'

const userOutput = ( props ) => {
    return (
        <div className="UserOutput">
            <p> Random text, {props.name} </p>
            <p> More Random text </p>
        </div>
    )
};

export default userOutput;