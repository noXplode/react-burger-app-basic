import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ValidationComponent from './ValidationComponent/ValidationComponent';
import CharComponent from './CharComponent/CharComponent';

class App extends Component {
  state = {
    text: '',
  }

  inputChangedHandler = (event) => {
    this.setState( {
      text: event.target.value
    } )

  }

  deleteLetterHandler = (index) => {
    let text = this.state.text.split('')
    delete text[index]
    text = text.join('')
    this.setState( {
      text: text
    } )

  }
  
  render() {
    return (
      <div className="App">
        <input type="text" onChange={this.inputChangedHandler} value={this.state.text} />
        <p> {this.state.length} </p>
        <ValidationComponent length={this.state.text.length}/>
        {this.state.text.split('').map((letter, index) => {
          return <CharComponent letter={letter} click={() => this.deleteLetterHandler(index)} />
        }
        )}
      </div>
    );
  }
}

export default App;
